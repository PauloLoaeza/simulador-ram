package actions;

import gui.Ventana;
import objects.RAM;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class DesfragmentarAction extends AbstractAction {

    public DesfragmentarAction(String texto) {
        super(texto);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RAM ram = RAM.getInstancia();
        ram.desfragmentar();

        Ventana.getInstancia().mostrarRAM();
    }
}
