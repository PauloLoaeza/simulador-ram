package actions;

import gui.Ventana;
import objects.Programa;
import objects.RAM;
import objects.ROM;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class EliminarProgramaAction extends AbstractAction {

    private JList<Programa> lista;
    private DefaultListModel<Programa> modelo;

    public EliminarProgramaAction(String texto, JList<Programa> lista) {
        super(texto);

        this.lista = lista;
        this.modelo = (DefaultListModel<Programa>) lista.getModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int indice = lista.getSelectedIndex();
        Ventana ventana = Ventana.getInstancia();

        if (indice == -1) {
            JOptionPane.showMessageDialog(ventana, "Seleccione un elemento");
            return;
        }

        RAM ram = RAM.getInstancia();
        Programa programa = lista.getSelectedValue();

        ROM rom = ROM.getInstancia();

        ram.eliminar(programa);
        modelo.remove(indice);
        ventana.mostrarRAM();
        ventana.cambiarEstatus(ram.memoriaDisponible(), rom.memoriaDisponible());
    }
}
