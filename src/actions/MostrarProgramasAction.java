package actions;

import util.Utilerias;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class MostrarProgramasAction extends AbstractAction {

    public MostrarProgramasAction(String texto) {
        super(texto);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Utilerias.mostrarProgramas();
    }
}
