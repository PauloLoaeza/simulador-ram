package actions;

import gui.Ventana;
import objects.Programa;
import objects.RAM;
import objects.ROM;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collections;
import java.util.List;

public class AbrirArchivoAction extends AbstractAction {

    public static final String[] EXTENSIONES_PERMITIDAS = {"txt"};

    public AbrirArchivoAction(String texto) {
        super(texto);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileFilter(new FileNameExtensionFilter("Archivos de texto", EXTENSIONES_PERMITIDAS));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        Ventana ventana = Ventana.getInstancia();

        int opcion = fileChooser.showOpenDialog(ventana);
        if (opcion != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File archivo = fileChooser.getSelectedFile();

        Programa programa = new Programa(archivo);

        RAM ram = RAM.getInstancia();
        ROM rom = ROM.getInstancia();

        if (ram.usar(programa)) {
            programa.setRam(true);
            ventana.agregarPrograma(programa);
            ventana.cambiarEstatus(ram.memoriaDisponible(), rom.memoriaDisponible());
            ventana.mostrarRAM();
            return;
        }

        List<Programa> programas = ram.getProgramas();
        Collections.sort(programas, Collections.reverseOrder());

        for (int i = 0; i < programas.size(); i++) {
            Programa p = programas.get(i);

            ram.eliminar(p);
            if (ram.usar(programa)) {
                programa.setRam(true);
                if(rom.usar(p)) {
                    p.setRam(false);
                    ventana.mostrarRAM();
                    ventana.agregarPrograma(programa);
                    ventana.cambiarEstatus(ram.memoriaDisponible(), rom.memoriaDisponible());
                    JOptionPane.showMessageDialog(ventana, "Programa: " + p + ", movido a rom");
                    return;
                } else {
                    ram.eliminar(programa);
                    programa.setRam(false);
                    ram.usar(p);
                    p.setRam(true);

                    ventana.mostrarRAM();
                    ventana.cambiarEstatus(ram.memoriaDisponible(), rom.memoriaDisponible());
                }
            } else {
                ram.usar(p);
                p.setRam(true);
                ventana.mostrarRAM();
                ventana.cambiarEstatus(ram.memoriaDisponible(), rom.memoriaDisponible());
            }
        }

        JOptionPane.showMessageDialog(ventana, "No hay espacio suficiente en la RAM y ROM");
    }
}
