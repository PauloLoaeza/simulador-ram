package actions;

import gui.Ventana;
import objects.Programa;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class DarControlAction extends AbstractAction {

    private JList<Programa> lista;
    private DefaultListModel<Programa> modelo;

    public DarControlAction(String texto, JList<Programa> lista) {
        super(texto);

        this.lista = lista;
        this.modelo = (DefaultListModel<Programa>) lista.getModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int indice = lista.getSelectedIndex();
        Ventana ventana = Ventana.getInstancia();

        if (indice == -1) {
            JOptionPane.showMessageDialog(ventana, "Seleccione un elemento");
            return;
        }

        Programa programa = lista.getSelectedValue();
        programa.setContador(programa.getContador() + 1);

        lista.validate();
        lista.repaint();
    }
}
