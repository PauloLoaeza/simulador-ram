package actions;

import gui.Ventana;
import objects.Programa;
import util.Utilerias;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

public class ExportarAction extends AbstractAction {

    private JList<Programa> lista;
    private DefaultListModel<Programa> modelo;

    public ExportarAction(String texto, JList<Programa> lista) {
        super(texto);
        this.lista = lista;
        this.modelo = (DefaultListModel<Programa>) lista.getModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int indice = lista.getSelectedIndex();
        Ventana ventana = Ventana.getInstancia();

        if (indice == -1) {
            JOptionPane.showMessageDialog(ventana, "Seleccione un elemento");
            return;
        }

        JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int opcion = fileChooser.showSaveDialog(ventana);
        if (opcion != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File archivo = fileChooser.getSelectedFile();
        if (!archivo.getName().endsWith(".txt")) {
            archivo = new File(archivo.getAbsolutePath() + ".txt");
        }

        if (archivo.exists()) {
            JOptionPane.showMessageDialog(ventana, "El archivo ya existe, elija otro nombre");
            return;
        }

        Utilerias.exportarArchivo(archivo, modelo.get(lista.getSelectedIndex()));
        JOptionPane.showMessageDialog(ventana, "Archivo exportado correctamente");
    }
}
