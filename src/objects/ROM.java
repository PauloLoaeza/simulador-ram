package objects;

import util.Utilerias;

import java.util.ArrayList;
import java.util.List;

public class ROM {

    public static final int MAX_MEMORIA = 3000;
    private static ROM instancia = null;

    private char[] memoria;
    private ArrayList<Programa> programas = new ArrayList<>();

    public static ROM getInstancia() {
        if (instancia == null) {
            synchronized (RAM.class) {
                if (instancia == null) {
                    instancia = new ROM();
                }
            }
        }

        return instancia;
    }

    private ROM() {
        memoria = new char[MAX_MEMORIA];
    }

    public boolean usar(Programa programa) {
        String texto = Utilerias.leerArchivo(programa.getArchivo(), false);
        programa.setTamanio(texto.length());

        char[] contenido = texto.toCharArray();

        if (memoriaDisponible() < contenido.length) {
            return false;
        }

        programas.add(programa);
        for (int i = 0, j = 0; i < memoria.length && j < contenido.length; i++) {
            if (memoria[i] == 0) {
                memoria[i] = contenido[j++];
                programa.agregarPosicion(i);
            }
        }

        return true;
    }

    public int memoriaDisponible() {
        int disponible = 0;

        for (int i = 0; i < memoria.length; i++) {
            if (memoria[i] == 0) disponible++;
        }

        return disponible;
    }

    public void eliminar(Programa programa) {
        ArrayList<Integer> posiciones = programa.getPosiciones();

        for (int i = 0; i < posiciones.size(); i++) {
            memoria[posiciones.get(i)] = 0;
        }

        programas.remove(programa);
    }

    public void desfragmentar() {
        limpiar();

        // Copiar programas
        List<Programa> programas = new ArrayList<>(this.programas.size());
        for (int i = 0; i < this.programas.size(); i++) {
            programas.add(this.programas.get(i));
        }

        this.programas = new ArrayList<>();
        for (int i = 0; i < programas.size(); i++) {
            usar(programas.get(i));
        }
    }

    private void limpiar() {
        memoria = new char[MAX_MEMORIA];
    }

    public ArrayList<Programa> getProgramas() {
        return programas;
    }

    @Override
    public String toString() {
        String str = "[";

        for (int i = 0; i < memoria.length; i++) {
            str += memoria[i] + "~";
        }

        // borra el último  caracter = '~'
        str = str.substring(0, str.length() -1);

        return str + "]";
    }
}
