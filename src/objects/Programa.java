package objects;

import java.io.File;
import java.util.ArrayList;

public class Programa implements Comparable<Programa> {

    private File archivo;
    private int tamanio;
    private int contador;
    private boolean ram;

    private ArrayList<Integer> posiciones = new ArrayList<>();

    public Programa(File archivo) {
        this.archivo = archivo;
    }

    public void agregarPosicion(int pos) {
        posiciones.add(pos);
    }

    public ArrayList<Integer> getPosiciones() {
        return posiciones;
    }

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public void setRam(boolean ram) {
        this.ram = ram;
    }

    public boolean isRam() {
        return ram;
    }

    @Override
    public String toString() {
        return archivo.getName() + ": [RAM: " + tamanio + ", control: " + contador + " ]";
    }

    @Override
    public int compareTo(Programa o) {
        if (contador < o.contador) {
            return -1;
        } else if (contador == o.contador) {
            if (tamanio < o.tamanio) {
                return -1;
            } else if (tamanio == o.tamanio) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}