package gui;

import gui.panels.ControlPanel;
import gui.panels.EstatusPanel;
import gui.panels.ProgramasPanel;
import gui.panels.RAMPanel;
import objects.Programa;

import javax.swing.*;
import java.awt.*;

public class Ventana extends JFrame {

    private static Ventana instancia = null;

    public static Ventana getInstancia() {
        if (instancia == null) {
            synchronized (Ventana.class) {
                if (instancia == null) {
                    instancia = new Ventana();
                }
            }
        }

        return instancia;
    }

    private ControlPanel pnlControl;
    private RAMPanel pnlRAM;
    private ProgramasPanel pnlProgramas;
    private EstatusPanel pnlEstaus;

    private Ventana() {
        super("Simulador de Memoria RAM");

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setExtendedState(MAXIMIZED_BOTH);
        setMinimumSize(new Dimension(950, 600));
        setLayout(new BorderLayout());

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        // Paneles
        pnlControl = new ControlPanel();
        pnlRAM = new RAMPanel();
        pnlProgramas = new ProgramasPanel();
        pnlEstaus = new EstatusPanel();

        // Agregar componentes
        add(pnlControl, BorderLayout.WEST);
        add(pnlRAM, BorderLayout.CENTER);
        add(pnlProgramas, BorderLayout.EAST);
        add(pnlEstaus, BorderLayout.NORTH);
    }

    public void mostrarRAM() {
        pnlRAM.mostrarRAM();
    }

    public void agregarPrograma(Programa programa) {
        pnlProgramas.agregarPrograma(programa);
    }

    public void cambiarEstatus(int ramDisponible, int romDisponible) {
        pnlEstaus.setRamDisponible(ramDisponible);
        pnlEstaus.setRomDisponible(romDisponible);
    }
}