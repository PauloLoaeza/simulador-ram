package gui.panels;

import actions.DarControlAction;
import actions.EliminarProgramaAction;
import actions.ExportarAction;
import objects.Programa;

import javax.swing.*;
import java.awt.*;

public class ProgramasPanel extends JPanel {

    private DefaultListModel<Programa> modelo;
    private JList<Programa> programaJList;

    private JButton btnEliminar;
    private JButton btnDarControl;
    private JButton btnExportar;

    public ProgramasPanel() {
        setBackground(Color.red);
        setPreferredSize(new Dimension(300, 100));
        setLayout(new BorderLayout());

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        modelo = new DefaultListModel<>();

        programaJList = new JList<>(modelo);
        programaJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        programaJList.setLayoutOrientation(JList.VERTICAL);

        // Acciones
        EliminarProgramaAction actEliminarPrograma =
                new EliminarProgramaAction("Eliminar", programaJList);
        DarControlAction actDarControl =
                new DarControlAction("Dar control", programaJList);
        ExportarAction actExportar =
                new ExportarAction("Exportar", programaJList);

        // Componentes
        JPanel pnlLabel = new JPanel(new FlowLayout());
        pnlLabel.add(new JLabel("Lista de programas: "));

        JPanel pnlBotones = new JPanel(new FlowLayout());
        btnEliminar = new JButton(actEliminarPrograma);
        btnDarControl = new JButton(actDarControl);
        btnExportar = new JButton(actExportar);
        pnlBotones.add(btnEliminar);
        pnlBotones.add(btnExportar);
        pnlBotones.add(btnDarControl);

        JScrollPane scrollPane = new JScrollPane(programaJList);

        // Agregar componentes
        add(pnlLabel, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
        add(pnlBotones, BorderLayout.SOUTH);
    }


    public void agregarPrograma(Programa programa) {
        modelo.addElement(programa);
    }
}
