package gui.panels;

import objects.RAM;
import objects.ROM;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class EstatusPanel extends JPanel {

    private int ramDisponible = RAM.MAX_MEMORIA;
    private int romDisponible = ROM.MAX_MEMORIA;

    private JLabel lblRam;
    private JLabel lblRom;

    public EstatusPanel() {
        setBorder(new LineBorder(Color.black));
        setPreferredSize(new Dimension(10, 30));

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        lblRam = new JLabel("R.A.M. disponible: " + ramDisponible);
        lblRom = new JLabel("R.O.M. disponible: " + romDisponible);
        add(lblRam);
        add(new JPanel());
        add(lblRom);
    }

    public void setRamDisponible(int ramDisponible) {
        this.ramDisponible = ramDisponible;
        lblRam.setText("R.A.M. disponible: " + ramDisponible);
    }

    public void setRomDisponible(int romDisponible) {
        this.romDisponible = romDisponible;
        lblRom.setText("R.O.M. disponible: " + romDisponible);
    }
}