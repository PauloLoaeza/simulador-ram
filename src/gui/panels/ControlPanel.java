package gui.panels;

import actions.AbrirArchivoAction;
import actions.DesfragmentarAction;
import actions.MostrarProgramasAction;

import javax.swing.*;
import java.awt.*;

public class ControlPanel extends JPanel {

    private JButton btnLeerArchivo;
    private JButton btnDesfragmentar;
    private JButton btnMostrar;

    public ControlPanel() {
        setPreferredSize(new Dimension(150, 25));

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        // Acciones / Listeners
        AbrirArchivoAction actAbrirArchivo = new AbrirArchivoAction("Abrir archivo");
        DesfragmentarAction actDesfragmentar = new DesfragmentarAction("Desfragmentar");
        MostrarProgramasAction actMostrarProgramas = new MostrarProgramasAction("Mostrar en consola");

        // Botones
        btnLeerArchivo = new JButton(actAbrirArchivo);
        btnDesfragmentar = new JButton(actDesfragmentar);
        btnMostrar = new JButton(actMostrarProgramas);

        btnLeerArchivo.setPreferredSize(getPreferredSize());
        btnDesfragmentar.setPreferredSize(getPreferredSize());
        btnMostrar.setPreferredSize(getPreferredSize());

        // Agregar componentes
        add(btnLeerArchivo);
        add(btnDesfragmentar);
        add(btnMostrar);
    }

}
