package gui.panels;

import objects.RAM;

import javax.swing.*;
import java.awt.*;

public class RAMPanel extends JPanel {

    private JScrollPane scrollPane;
    private JTextArea txtArea;

    public RAMPanel() {
        setLayout(new BorderLayout());

        iniciarComponentes();

        mostrarRAM();
    }

    private void iniciarComponentes() {
        txtArea = new JTextArea(10, 10);
        scrollPane = new JScrollPane(txtArea);

        txtArea.setLineWrap(true);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        add(scrollPane, BorderLayout.CENTER);
    }

    public void mostrarRAM() {
        txtArea.setText(RAM.getInstancia().toString());
    }

}
