package util;

import gui.Ventana;
import objects.Programa;
import objects.RAM;

import javax.swing.*;
import java.io.*;
import java.util.List;

public class Utilerias {

    public static String leerArchivo(File archivo, boolean export) {
        FileReader fr = null;
        BufferedReader br = null;

        String resultado = "";

        try {
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            for (int c = br.read(); c != -1; c = br.read()) {
                if (export || c != 13) {
                    resultado += (char) c;
                }
            }

        } catch (IOException e) {
            JOptionPane.showMessageDialog(Ventana.getInstancia(),
                    "No se puedo leer archivo");
        } finally {

            try {
                if (br != null) {
                    br.close();
                }
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(Ventana.getInstancia(),
                        "El archivo no se cerró correctamente");
            }

            return resultado;
        }
    }

    public static void exportarArchivo(File archivo, Programa programa) {
        FileWriter rw = null;
        BufferedWriter bw = null;

        try {
            rw = new FileWriter(archivo);
            bw = new BufferedWriter(rw);

            bw.write(Utilerias.leerArchivo(programa.getArchivo(), true));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(Ventana.getInstancia(), "Error al exportar archivo");
        } finally {

            try {
                if (bw != null) {
                    bw.close();
                }

                if (rw != null) {
                    rw.close();
                }
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(Ventana.getInstancia(), "Error al cerrar archivo");
            }

        }
    }

    public static void mostrarProgramas() {
        List<Programa> programas = RAM.getInstancia().getProgramas();
        for (int i = 0; i < programas.size(); i++) {
            System.out.println("Programa " + i + ": " +
                    programas.get(i).getArchivo().getName() +
                    " = [" + Utilerias.leerArchivo(programas.get(i).getArchivo(), false) + "]\n");
        }
    }
}
