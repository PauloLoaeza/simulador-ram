package main;

import gui.Ventana;

import javax.swing.*;

public class Simulador {
    public static void main(String... args) {
        SwingUtilities.invokeLater(() -> {
            Ventana ventana = Ventana.getInstancia();
            ventana.setVisible(true);
        });
    }
}